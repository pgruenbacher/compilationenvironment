. bashrc
PREFIX=$INSTALL_ROOT/ffmpeg
VERSION="${VERSION:-2.5.2}"

. $TOOLS_HOME/setup.sh
cd $BUILD_DIR

[[ ! -e $DOWNLOADS_DIR/x264-snapshot-20141218-2245.tar.bz2 ]] && wget -P $DOWNLOADS_DIR http://download.videolan.org/x264/snapshots/x264-snapshot-20141218-2245.tar.bz2
tar -xjf $DOWNLOADS_DIR/x264-snapshot-20141218-2245.tar.bz2
cd x264-snapshot-20141218-2245
./configure --prefix=$PREFIX --enable-shared
make -j3
make install
cd ..
rm -rf x264-snapshot-20141218-2245

[[ ! -e $DOWNLOADS_DIR/ffmpeg-${VERSION}.tar.bz2 ]] && wget -P $DOWNLOADS_DIR http://ffmpeg.org/releases/ffmpeg-${VERSION}.tar.bz2
tar -xjf $DOWNLOADS_DIR/ffmpeg-${VERSION}.tar.bz2
cd ffmpeg-${VERSION}
./configure --prefix=$PREFIX --enable-gpl --enable-version3 --enable-libx264 --disable-debug --disable-static --disable-doc --enable-shared
make -j3
make install
cd ..
rm -rf ffmpeg-${VERSION}
