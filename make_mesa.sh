. bashrc
PREFIX=$INSTALL_ROOT/mesa
VERSION="${VERSION:-10.4.2}"

. $TOOLS_HOME/setup.sh
cd $BUILD_DIR

[[ ! -e $DOWNLOADS_DIR/MesaLib-${VERSION}.tar.bz2 ]] && wget -P $DOWNLOADS_DIR ftp://ftp.freedesktop.org/pub/mesa/${VERSION}/MesaLib-${VERSION}.tar.bz2
tar -xjf $DOWNLOADS_DIR/MesaLib-${VERSION}.tar.bz2
cd Mesa-${VERSION}
./configure CXXFLAGS="-fno-rtti -O2 -DDEFAULT_SOFTWARE_DEPTH_BITS=31" CFLAGS="-O2 -DDEFAULT_SOFTWARE_DEPTH_BITS=31" --disable-xvmc --disable-glx --disable-dri --with-dri-drivers= --with-gallium-drivers=swrast --enable-texture-float --disable-shared-glapi --disable-egl --with-egl-platforms= --enable-gallium-osmesa --enable-gallium-llvm=yes --disable-static --prefix=$PREFIX
make -j3
make install
cd ..
rm -rf Mesa-${VERSION}

[[ ! -e $DOWNLOADS_DIR/glu-9.0.0.tar.bz2 ]] && wget -P $DOWNLOADS_DIR ftp://ftp.freedesktop.org/pub/mesa/glu/glu-9.0.0.tar.bz2
tar -xjf $DOWNLOADS_DIR/glu-9.0.0.tar.bz2
cd glu-9.0.0
./configure --prefix=$PREFIX --disable-static
make -j3
make install
cd ..
rm -rf glu-9.0.0
