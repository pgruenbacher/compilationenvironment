. bashrc
echo BUILD_ROOT: $BUILD_ROOT
[[ ! -d $BUILD_ROOT ]] && mkdir -p $BUILD_ROOT
[[ ! -d $BUILD_DIR ]] && mkdir -p $BUILD_DIR
[[ ! -d $TOOLS_HOME ]] && mkdir -p $TOOLS_HOME

cp -a overlay/Tools/* $TOOLS_HOME/

. $TOOLS_HOME/setup.sh

cd $BUILD_DIR

echo Current GCC version
gcc -v
#exit

[[ ! -e $DOWNLOADS_DIR/patchelf-0.8.tar.bz2 ]] && wget -P $DOWNLOADS_DIR http://nixos.org/releases/patchelf/patchelf-0.8/patchelf-0.8.tar.bz2
tar -xjf $DOWNLOADS_DIR/patchelf-0.8.tar.bz2
cd patchelf-0.8
./configure --prefix=$TOOLS_HOME
make $MAKEOPTS
make install
cd ..
rm -rf patchelf-0.8

[[ ! -e $DOWNLOADS_DIR/git-2.2.1.tar.gz ]] && wget -P $DOWNLOADS_DIR https://www.kernel.org/pub/software/scm/git/git-2.2.1.tar.gz
tar -zxf $DOWNLOADS_DIR/git-2.2.1.tar.gz
cd git-2.2.1
./configure --prefix=$TOOLS_HOME
make $MAKEOPTS
make install
cd ..
rm -rf git-2.2.1

tar -zxf $DOWNLOADS_DIR/autoconf-2.69.tar.gz
cd autoconf-2.69
./configure --prefix=$TOOLS_HOME
make $MAKEOPTS
make install
cd ..
rm -rf autoconf-2.69

tar -zxf $DOWNLOADS_DIR/automake-1.14.tar.gz
cd automake-1.14
./configure --prefix=$TOOLS_HOME
make $MAKEOPTS
make install
cd ..
rm -rf automake-1.14

tar -zxf $DOWNLOADS_DIR/libtool-2.4.2.tar.gz
cd libtool-2.4.2
./configure --prefix=$TOOLS_HOME
make $MAKEOPTS
make install
cd ..
rm -rf libtool-2.4.2

tar -xjf $DOWNLOADS_DIR/flex-2.5.39.tar.bz2
cd flex-2.5.39
./configure --prefix=$TOOLS_HOME
make $MAKEOPTS
make install
cd ..
rm -rf flex-2.5.39

[[ ! -e $DOWNLOADS_DIR/cmake-3.1.0.tar.gz ]] && wget -P $DOWNLOADS_DIR http://www.cmake.org/files/v3.1/cmake-3.1.0.tar.gz
tar -zxf $DOWNLOADS_DIR/cmake-3.1.0.tar.gz
cd cmake-3.1.0
./configure --prefix=$TOOLS_HOME
gmake $MAKEOPTS
gmake install
cd ..
rm -rf cmake-3.1.0

tar -zxf $DOWNLOADS_DIR/yasm-1.2.0.tar.gz
cd yasm-1.2.0
./configure --prefix=$TOOLS_HOME
make $MAKEOPTS
make install
cd ..
rm -rf yasm-1.2.0

[[ ! -e $DOWNLOADS_DIR/Python-2.7.9.tgz ]] && wget -P $DOWNLOADS_DIR https://www.python.org/ftp/python/2.7.9/Python-2.7.9.tgz
tar -zxf $DOWNLOADS_DIR/Python-2.7.9.tgz
cd Python-2.7.9
./configure --prefix=$TOOLS_HOME --enable-shared --with-ensurepip=install
make $MAKEOPTS
make install
cd ..
rm -rf Python-2.7.9

pip install virtualenv -U
pip install -U setuptools

tar -zxf $DOWNLOADS_DIR/llvm-3.5.0.src.tar.gz
mkdir llvm-3.5.0.build
cd llvm-3.5.0.build
cmake ../llvm-3.5.0.src -DBUILD_SHARED_LIBS:BOOL=ON -DCMAKE_BUILD_TYPE:STRING=Release  -DCMAKE_INSTALL_PREFIX:PATH=$TOOLS_HOME
make $MAKEOPTS
make install
cd ..
rm -rf llvm-3.5.0.build  llvm-3.5.0.src

tar -xjf $DOWNLOADS_DIR/openmpi-1.4.3.tar.bz2
cd openmpi-1.4.3
./configure --prefix=$TOOLS_HOME
make $MAKEOPTS
make install
cd ..
rm -rf openmpi-1.4.3

tar -zxf $DOWNLOADS_DIR/libffi-3.1.tar.gz
cd libffi-3.1
./configure --prefix=$TOOLS_HOME
make $MAKEOPTS
make install
cd ..
rm -rf libffi-3.1

echo "Completed"
echo "You must add . $TOOLS_HOME/setup.sh to your .bashrc file"
