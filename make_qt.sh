. bashrc
VERSION="${VERSION:-4.8.6}"
MINOR_VERSION=${VERSION:0:3}
MAJOR_VERSION=${VERSION:0:1}
PREFIX=$INSTALL_ROOT/qt${MAJOR_VERSION}
. $TOOLS_HOME/setup.sh
cd $BUILD_DIR
[[ ! -e $DOWNLOADS_DIR/qt-everywhere-opensource-src-${VERSION}.tar.gz ]] && wget --no-check-certificate -P $DOWNLOADS_DIR http://download.qt.io/official_releases/qt/${MINOR_VERSION}/${VERSION}/single/qt-everywhere-opensource-src-${VERSION}.tar.gz
tar -zxf $DOWNLOADS_DIR/qt-everywhere-opensource-src-${VERSION}.tar.gz
cd qt-everywhere-opensource-src-${VERSION}
if [[ $MAJOR_VERSION == "4" ]]; then
    ./configure -prefix $PREFIX -opensource -confirm-license -no-cups -nomake demos -nomake examples -fast -nomake docs -no-rpath -no-xinerama -no-glib \
        -nomake translations -no-declarative-debug -nomake assistant -qt-libpng -qt-libjpeg -no-phonon -optimized-qmake -stl -largefile -system-sqlite -no-openvg  \
        -no-multimedia -no-dbus -no-audio-backend -release -libdir $PREFIX/lib -plugindir $PREFIX/lib/plugins -no-gtkstyle # -make libs
else
#    ./configure -prefix $PREFIX -opensource -confirm-license -no-cups -nomake examples -no-rpath \
#        -qt-libpng -qt-libjpeg -no-dbus -no-audio-backend -release -libdir $PREFIX/lib -plugindir $PREFIX/lib/plugins
    ./configure -prefix $PREFIX -opensource -confirm-license -release -make libs -no-separate-debug-info -shared -largefile -accessibility -no-sql-db2 -no-sql-ibase -no-sql-mysql -no-sql-oci -no-sql-odbc -no-sql-psql -no-sql-sqlite -no-sql-sqlite2 -no-sql-tds -pkg-config -system-zlib -no-mtdev \
                -no-journald -no-audio-backend \
                -no-libpng -no-libjpeg \
                -no-freetype -no-harfbuzz \
                -no-openssl \
                -no-xinput2 -no-xcb-xlib -no-pulseaudio -no-alsa -no-gtkstyle -nomake examples \
                -nomake tests \
-no-compile-examples -no-cups -no-evdev -no-icu -no-fontconfig -no-dbus -no-xcb -no-eglfs -no-directfb -no-linuxfb -no-kms -no-xkb -no-xrender \
-no-mitshm -no-xcursor -no-xfixes -no-xinerama -no-xinput -no-xrandr -no-xshape -no-xsync -no-xvideo -sm -qreal double -no-warnings-are-errors
fi
make $MAKEOPTS
make install
cd ..
#rm -rf qt-everywhere-opensource-src-${VERSION}
