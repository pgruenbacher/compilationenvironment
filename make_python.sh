. bashrc
PYTHON_VERSION="${VERSION:-2.7.9}"
PREFIX=$INSTALL_ROOT/python

#
# A basic python distribution
#
. $TOOLS_HOME/setup.sh
add_path ${PREFIX}/bin/
add_library_path ${PREFIX}/lib

function build_python() {
    cd $BUILD_DIR
    [[ ! -e $DOWNLOADS_DIR/Python-${PYTHON_VERSION}.tgz ]] && wget -P $DOWNLOADS_DIR https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz
    tar -zxf $DOWNLOADS_DIR/Python-${PYTHON_VERSION}.tgz
    cd Python-${PYTHON_VERSION}
    sed -i "s#/usr/contrib/ssl#${PREFIX}#g" setup.py
    LDFLAGS='-Wl,-rpath=\$$ORIGIN/../lib'  ./configure --enable-shared --prefix=$PREFIX --with-ensurepip=install
    make $MAKEOPTS
    make install
    cd ..
    rm -rf Python-${PYTHON_VERSION}
}

function install_requirements() {
    # Now setup all the requirements
cat > requirements.txt<< EOF
virtualenv
#Cython==0.21.2
Fabric==1.10.1
Pillow==2.7.0
numpy==1.9.1
scipy==0.15.1
configobj==5.0.6
cvxopt==1.1.7
ipython==2.3.1
matplotlib==1.4.2
mpi4py==1.3.1
openpyxl==2.1.4
pandas==0.15.2
psycopg2==2.5.4
python-dateutil==2.4
requests==2.5.1
psutil==2.2.1
pyparsing==2.0.3
ply==3.4
PyYaml==3.11
Jinja2==2.7.3
pathlib==1.0.1
MySQL-python==1.2.5
traits==4.5.0
EOF

#    $PREFIX/bin/pip install -U pip setuptools
    $PREFIX/bin/pip install -r requirements.txt && rm -f requirements.txt
    $PREFIX/bin/pip install git+ssh://git@bitbucket.org/totalsim/transformations.git@v1.0.2#egg=transformations
}

function tidy() {
    PREFIX=python
    DIST_DIR=$DIST_ROOT/$PREFIX
    INSTALL_DIR=${INSTALL_ROOT}/$PREFIX
    mkdir -p $DIST_DIR/
    echo $INSTALL_DIR $DIST_DIR/
    cp -a $INSTALL_DIR/lib $DIST_DIR/
    cp -a $INSTALL_DIR/bin $DIST_DIR/
    [[ -d overlay/$PREFIX ]] && rsync -a overlay/$PREFIX/ $DIST_DIR/
    # Copy over MPI libs
    cp $TOOLS_HOME/lib/libmpi*.so* $DIST_DIR/lib
    cp $TOOLS_HOME/lib/libopen-*.so* $DIST_DIR/lib
    mkdir $DIST_DIR/lib/openmpi
    cp $TOOLS_HOME/lib/openmpi/*.so* $DIST_DIR/lib/openmpi/
    cp /opt/centos/devtoolset-2/root/usr/lib/gcc/x86_64-redhat-linux/4.8.2/libgfortran.so $DIST_DIR/lib

    $INSTALL_DIR/bin/virtualenv --relocatable $DIST_DIR
    find $DIST_DIR -name '*.a' | xargs rm -f
    find $DIST_DIR -name '*.la' | xargs rm -f
    find $DIST_DIR -name '*.pyc' | xargs rm -f

    BASE_DIR=$PWD

    cd $DIST_DIR/bin
    rm -f my* pil* libpng* openssl msql2mysql drop* perror pg* create* c_rehash inno* pg_* png* *db resolve* replace
}


build_python
install_requirements
