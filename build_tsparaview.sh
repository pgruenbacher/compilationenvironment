#!/bin/bash
. bashrc
VERSION=4.3.1
NAME=TSParaview VERSION=$VERSION DO_QT=TRUE DO_PARAVIEW=TRUE DO_PYTHON=TRUE ./make_dist.sh
make_relocatable /build/Dist/TSParaview-v$VERSION

function make_codehome_tarball() {
    echo "Make CODEHOME tarball"
    local home=$1
    local app=$2
    local version=$3
    tmp=tmp$$
    local app_path=$tmp/codes/$app/$app-$version

    mkdir -p $tmp/codes/bin
    mkdir -p $app_path
    rsync -a $home/ $app_path

cat > $tmp/codes/bin/paraview-v$VERSION << EOF
#!/bin/bash
exec $TS_CODEHOME/TSrel/codes/TSParaview/TSParaview-v4.3.1/bin/paraview $@
EOF
cat > $tmp/codes/bin/pvbatch-v$VERSION << EOF
#!/bin/bash
exec $TS_CODEHOME/TSrel/codes/TSParaview/TSParaview-v4.3.1/bin/pvbatch $@
EOF
cat > $tmp/codes/bin/pvserver-v$VERSION << EOF
#!/bin/bash
exec $TS_CODEHOME/TSrel/codes/TSParaview/TSParaview-v4.3.1/bin/pvserver $@
EOF
cat > $tmp/codes/bin/pvpython-v$VERSION << EOF
#!/bin/bash
exec $TS_CODEHOME/TSrel/codes/TSParaview/TSParaview-v4.3.1/bin/pvpython $@
EOF

    tarball=$app-$version.tbz2
    (cd $tmp && tar -cjf $tarball codes)
    mv $tmp/$tarball $RELEASE_ROOT && rm -rf $tmp
}



make_codehome_tarball $DIST_ROOT/TSParaview-v$VERSION TSParaview v$VERSION
