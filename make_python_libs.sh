. bashrc
PREFIX=$INSTALL_ROOT/python

#
# A basic python distribution
#
. $TOOLS_HOME/setup.sh
add_path ${PREFIX}/bin/
add_library_path ${PREFIX}/lib

function build_libraries() {
    # Install any library dependencies
    cd $BUILD_DIR
    [[ ! -e $DOWNLOADS_DIR/readline-6.3.tar.gz ]] && wget -P $DOWNLOADS_DIR ftp://ftp.cwru.edu/pub/bash/readline-6.3.tar.gz
    tar -zxf $DOWNLOADS_DIR/readline-6.3.tar.gz
    cd readline-6.3
    ./configure --prefix=$PREFIX
    make $MAKEOPTS
    make install
    cd ..
    rm -rf readline-6.3

    [[ ! -e $DOWNLOADS_DIR/libpng-1.6.15.tar.gz ]] && wget -P $DOWNLOADS_DIR http://prdownloads.sourceforge.net/libpng/libpng-1.6.15.tar.gz
    tar -zxf $DOWNLOADS_DIR/libpng-1.6.15.tar.gz
    cd libpng-1.6.15
    ./configure --prefix=$PREFIX
    make $MAKEOPTS
    make install
    cd ..
    rm -rf libpng-1.6.15

    [[ ! -e $DOWNLOADS_DIR/freetype-2.5.5.tar.bz2 ]] && wget -P $DOWNLOADS_DIR http://download.savannah.gnu.org/releases/freetype/freetype-2.5.5.tar.bz2
    tar -xjf $DOWNLOADS_DIR/freetype-2.5.5.tar.bz2
    cd freetype-2.5.5
    ./configure --prefix=$PREFIX --enable-static=no
    make $MAKEOPTS
    make install
    cd ..
    rm -rf freetype-2.5.5

    [[ ! -e $DOWNLOADS_DIR/openssl-1.0.1j.tar.gz ]] && wget -P $DOWNLOADS_DIR https://www.openssl.org/source/openssl-1.0.1j.tar.gz
    tar -zxf $DOWNLOADS_DIR/openssl-1.0.1j.tar.gz
    cd openssl-1.0.1j
    ./config --openssldir=$PREFIX shared
    make $MAKEOPTS
    make install
    cd ..
    rm -rf openssl-1.0.1j

    [[ ! -e $DOWNLOADS_DIR/lapack-3.5.0.tgz ]] && wget -P $DOWNLOADS_DIR http://www.netlib.org/lapack/lapack-3.5.0.tgz
    tar -zxf $DOWNLOADS_DIR/lapack-3.5.0.tgz
    cd lapack-3.5.0
    cmake . -DCMAKE_INSTALL_PREFIX:PATH=$PREFIX -DBUILD_SHARED_LIBS:BOOL=ON -DBUILD_TESTING:BOOL=OFF -DCMAKE_BUILD_TYPE:STRING=Release
    make $MAKEOPTS
    make install
    cd ..
    rm -rf lapack-3.5.0
     
    [[ ! -e $DOWNLOADS_DIR/mariadb-10.0.16.tar.gz ]] && wget -P $DOWNLOADS_DIR https://downloads.mariadb.org/interstitial/mariadb-10.0.16/source/mariadb-10.0.16.tar.gz
    tar -zxf $DOWNLOADS_DIR/mariadb-10.0.16.tar.gz
    cd mariadb-10.0.16
    ./BUILD/autorun.sh
    cmake . -DCMAKE_INSTALL_PREFIX:PATH=$PREFIX -DWITHOUT_SERVER:BOOL=ON -DCMAKE_BUILD_TYPE:STRING=Release -DWITH_UNIT_TESTS:BOOL=OFF
    make $MAKEOPTS
    make install
    cd ..
    rm -rf mariadb-10.0.16

    [[ ! -e $DOWNLOADS_DIR/postgresql-9.4.0.tar.bz2 ]] && wget -P $DOWNLOADS_DIR https://ftp.postgresql.org/pub/source/v9.4.0/postgresql-9.4.0.tar.bz2
    tar -xjf $DOWNLOADS_DIR/postgresql-9.4.0.tar.bz2
    cd postgresql-9.4.0
    ./configure --prefix=$PREFIX --without-readline
    make $MAKEOPTS
    gmake -C src/bin install
    gmake -C src/include install
    gmake -C src/interfaces install
    gmake -C doc install
    cd ..
    rm -rf postgresql-9.4.0

    [[ ! -e $DOWNLOADS_DIR/sqlite-autoconf-3080704.tar.gz ]] && wget -P $DOWNLOADS_DIR http://www.sqlite.org/2014/sqlite-autoconf-3080704.tar.gz
    tar -zxf $DOWNLOADS_DIR/sqlite-autoconf-3080704.tar.gz
    cd sqlite-autoconf-3080704
    ./configure --prefix=$PREFIX
    make $MAKEOPTS
    make install
    cd ..
    rm -rf sqlite-autoconf-3080704/
}

build_libraries
