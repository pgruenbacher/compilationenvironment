"""
    site_config/settings.py

    Copyright TotalSim Ltd, 2014 all rights reserved
        M. Layton (mlayton@totalsim.co.uk)

    The contents of this file are NOT for redistribution
    Please see the README.TotalSim file distributed with this source code
"""
import os
CWD = os.path.dirname(os.path.abspath(__file__))
