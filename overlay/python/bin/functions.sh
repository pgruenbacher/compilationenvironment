#!/bin/bash
#
#   Basic functions for dealing with PATH, LD_LIBRARY_PATH & PYTHONPATH
#
#   Copyright TotalSim Ltd, 2009-2014 all rights reserved
#       M Layton (mlayton@totalsim.co.uk)
#
#   The contents of this file are NOT for redistribution
#   Please see the README.TotalSim file distributed with this source code
#
function add_path() {
    # Add a directory to the $PATH
    if ! echo $PATH | egrep -q "(^|:)$1($|:)" ; then
        if [ "$2" = "after" ] ; then
            PATH=$PATH:$1
        else
            PATH=$1:$PATH
        fi
    fi
    export PATH
}

function add_library_path() {
    # Add a directory to the $LD_LIBRARY_PATH
    if ! echo $LD_LIBRARY_PATH | egrep -q "(^|:)$1($|:)" ; then
        if [[ -z $LD_LIBRARY_PATH ]]; then
            export LD_LIBRARY_PATH=$1
        else
            export LD_LIBRARY_PATH="$1:"$LD_LIBRARY_PATH
        fi
    fi
}

function add_python_path() {
    # Add a directory to the $PYTHONPATH
    if ! echo $PYTHONPATH | /bin/egrep -q "(^|:)$1($|:)" ; then
        if [[ -z $PYTHONPATH ]]; then
            export PYTHONPATH=$1
        else
            export PYTHONPATH=$1:$PYTHONPATH
        fi
    fi
}

function load_module() {
    # Load a setup script from a module
    local path=$1
    if [[ -e ${path}/setup.sh ]]; then
        . ${path}/setup.sh
    elif [[ -d ${path} ]]; then
        [[ -d ${path}/lib ]] && add_library_path ${path/lib}
        [[ -d ${path}/bin ]] && add_path ${path/bin}
    fi
}