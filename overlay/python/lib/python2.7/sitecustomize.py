"""
    sitecustomize.py
    - Startup routines to add additional application paths to the PYTHONPATH

    Copyright TotalSim Ltd, 2015 all rights reserved
        M. Layton (mlayton@totalsim.co.uk)

    The contents of this file are NOT for redistribution
    Please see the README.TotalSim file distributed with this source code
"""
import os, site

# Add the site-packages folder into the site
app_home = os.getenv('TS_APPLICATION_HOME')
if app_home:
    site_packages = app_home + os.path.sep + 'site-packages'
    if os.path.isdir(site_packages):
        site.addsitedir(site_packages)
