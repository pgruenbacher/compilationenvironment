#!/bin/bash
#
#   Setup routine
#
#   Copyright TotalSim Ltd, 2014 all rights reserved
#       M Layton (mlayton@totalsim.co.uk)
#
#   The contents of this file are NOT for redistribution
#   Please see the README.TotalSim file distributed with this source code
#
path=$(dirname ${BASH_SOURCE[0]})
export TS_PYTHON_HOME=$(readlink -f $path)
export PYTHONHOME=$TS_PYTHON_HOME
[[ -e $TS_PYTHON_HOME/bin/functions.sh ]] && . $TS_PYTHON_HOME/bin/functions.sh

add_library_path $TS_PYTHON_HOME/lib
add_library_path $TS_PYTHON_HOME/lib/python2.7/site-packages

add_library_path $TS_PYTHON_HOME/lib/paraview-4.3

add_path $TS_PYTHON_HOME/bin
add_python_path $TS_PYTHON_HOME
