#!/bin/bash
function add_path() {
    # Add a directory to the $PATH
    if ! echo $PATH | egrep -q "(^|:)$1($|:)" ; then
        if [ "$2" = "after" ] ; then
            PATH=$PATH:$1
        else
            PATH=$1:$PATH
        fi
    fi
    export PATH
}

function add_library_path() {
    # Add a directory to the $LD_LIBRARY_PATH
    if ! echo $LD_LIBRARY_PATH | egrep -q "(^|:)$1($|:)" ; then
        if [[ -z $LD_LIBRARY_PATH ]]; then
            export LD_LIBRARY_PATH=$1
        else
            export LD_LIBRARY_PATH="$1:"$LD_LIBRARY_PATH
        fi
    fi
}

path=$(dirname ${BASH_SOURCE[0]})
export TS_DEV_HOME=$(readlink -f $path)

add_path $TS_DEV_HOME/bin
add_library_path $TS_DEV_HOME/lib

export ACLOCAL="aclocal -I/usr/share/aclocal"
