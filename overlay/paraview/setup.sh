#!/bin/bash

function add_path() {
    # Add a directory to the $PATH
    if ! echo $PATH | egrep -q "(^|:)$1($|:)" ; then
        if [ "$2" = "after" ] ; then
            PATH=$PATH:$1
        else
            PATH=$1:$PATH
        fi
    fi
    export PATH
}

function add_library_path() {
    # Add a directory to the $LD_LIBRARY_PATH
    if ! echo $LD_LIBRARY_PATH | egrep -q "(^|:)$1($|:)" ; then
        if [[ -z $LD_LIBRARY_PATH ]]; then
            export LD_LIBRARY_PATH=$1
        else
            export LD_LIBRARY_PATH="$1:"$LD_LIBRARY_PATH
        fi
    fi
}

function add_python_path() {
    # Add a directory to the $PYTHONPATH
    if ! echo $PYTHONPATH | /bin/egrep -q "(^|:)$1($|:)" ; then
       	if [[ -z $PYTHONPATH ]]; then
            export PYTHONPATH=$1
       	else
            export PYTHONPATH=$1:$PYTHONPATH
       	fi
    fi
}

path=$(dirname ${BASH_SOURCE[0]})
export TS_RENDERER_HOME=$(readlink -f $path)

add_library_path $TS_RENDERER_HOME/lib/
add_library_path $TS_RENDERER_HOME/lib/paraview-*
add_path $TS_RENDERER_HOME/bin

# Setup the python paths
