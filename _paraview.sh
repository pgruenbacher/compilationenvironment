. bashrc
VERSION="${VERSION:-4.1.0}"
PREFIX=$INSTALL_ROOT/paraview_$TARGET-v$VERSION
PLUGINS_VERSION="${PLUGINS_VERSION:-1.1}"
DISABLE_PLUGINS="${DISABLE_PLUGINS:-FALSE}"
echo "Compiling ParaView v$VERSION ($TARGET)"

. $TOOLS_HOME/setup.sh
cd $BUILD_DIR

tar -zxf $DOWNLOADS_DIR/ParaView-v${VERSION}-source.tar.gz

mkdir paraview_${TARGET}_build
SOURCE_DIR=$BUILD_DIR/ParaView-v${VERSION}-source
PLUGIN_SOURCE_DIR="$BUILD_DIR/TSPlugins"
mkdir $PLUGIN_SOURCE_DIR
tar -xjf $DOWNLOADS_DIR/TS-ParaView-Plugins-v${PLUGINS_VERSION}.tbz2 -C $PLUGIN_SOURCE_DIR

MESA_DIR="$INSTALL_ROOT/mesa"
QT_DIR="$INSTALL_ROOT/qt4"

add_library_path $MESA_DIR/lib
add_path $QT_DIR/bin
add_library_path $QT_DIR/lib

main_opts="-DCMAKE_INSTALL_PREFIX:PATH=${PREFIX} \
                        -DCMAKE_BUILD_TYPE:STRING=Release \
                        -DBUILD_DOCUMENTATION:BOOL=OFF \
                        -DBUILD_TESTING:BOOL=OFF \
                        -DPARAVIEW_USE_MPI:BOOL=ON \
                        -DPARAVIEW_ENABLE_WEB:BOOL=OFF \
                        -DPARAVIEW_ENABLE_PYTHON:BOOL=ON \
                        -DPARAVIEW_ENABLE_FFMPEG:BOOL=OFF \
                        -DMPIEXEC_MAX_NUMPROCS:STRING=96 \
                        -DVTK_MPI_MAX_NUMPROCS:STRING=96 \
                        -DPARAVIEW_ENABLE_CATALYST:BOOL=OFF \
                        -DPARAVIEW_BUILD_CATALYST_ADAPTORS:BOOL=OFF \
                        -DPARAVIEW_BUILD_PLUGIN_GMVReader:BOOL=OFF \
                        -DPARAVIEW_BUILD_PLUGIN_MantaView:BOOL=OFF \
                        -DPARAVIEW_BUILD_PLUGIN_EyeDomeLighting:BOOL=ON \
                        -DPARAVIEW_BUILD_PLUGIN_ArrowGlyph:BOOL=ON \
                        -DPARAVIEW_BUILD_PLUGIN_PointSprite:BOOL=ON \
                        -DPARAVIEW_BUILD_PLUGIN_ForceTime:BOOL=ON \
                        -DPARAVIEW_BUILD_PLUGIN_H5PartReader:BOOL=OFF \
                        -DPARAVIEW_BUILD_PLUGIN_SierraPlotTools:BOOL=OFF \
                        -DPARAVIEW_BUILD_PLUGIN_SLACTools:BOOL=OFF \
                        -DPARAVIEW_BUILD_PLUGIN_PacMan:BOOL=OFF \
                        -DPARAVIEW_BUILD_PLUGIN_SciberQuestToolKit:BOOL=OFF \
                        -DPARAVIEW_BUILD_PLUGIN_UncertaintyRendering:BOOL=OFF \
                        -DPARAVIEW_BUILD_PLUGIN_AnalyzeNIfTIIO:BOOL=OFF \
                        -DPARAVIEW_BUILD_PLUGIN_RGBZView:BOOL=OFF \
                        -DSURFACELIC_PLUGIN_TESTING:BOOL=OFF \
                        -DModule_vtkUtilitiesProcessXML:BOOL=ON \
                        -DModule_vtkPVServerManagerApplication:BOOL=ON"

mpi_opts="-DPARAVIEW_USE_MPI:BOOL=ON \
          -DMPIEXEC_MAX_NUMPROCS:STRING=96 \
          -DVTK_MPI_MAX_NUMPROCS:STRING=96"

ui_opts="-DPARAVIEW_BUILD_QT_GUI:BOOL=ON -DPARAVIEW_QT_VERSION:STRING=4 -DQT_QMAKE_EXECUTABLE:PATH=$QT_DIR/bin/qmake"
batch_opts="-DPARAVIEW_BUILD_QT_GUI:BOOL=OFF"
web_opts="-DPARAVIEW_ENABLE_WEB:BOOL=ON"

mesa_opts="-DVTK_OPENGL_HAS_OSMESA:BOOL=ON \
                -DOPENGL_INCLUDE_DIR:PATH=${MESA_DIR}/include \
                -DOPENGL_gl_LIBRARY:FILEPATH= \
                -DOPENGL_glu_LIBRARY:FILEPATH=${MESA_DIR}/lib/libGLU.so \
                -DOSMESA_INCLUDE_DIR:PATH=${MESA_DIR}/include \
                -DOSMESA_LIBRARY:FILEPATH=${MESA_DIR}/lib/libOSMesa.so \
                -DVTK_USE_X:BOOL=OFF \
                -DVTK_USE_OFFSCREEN:BOOL=ON"

plugin_opts="-DPARAVIEW_EXTERNAL_PLUGIN_DIRS:PATH=${PLUGIN_SOURCE_DIR}/vtkMSHReader;${PLUGIN_SOURCE_DIR}/vtkPOFFReader"

mesa_batch_opts="$main_opts $mpi_opts $mesa_opts $batch_opts"
batch_opts="$main_opts $mpi_opts $batch_opts"
gui_opts="$main_opts $ui_opts $mpi_opts"

opts=$(eval echo \$${TARGET}_opts)

if [[ $DISABLE_PLUGINS == FALSE ]]; then
    echo "Using TS Plugins"
    opts="$opts $plugin_opts"
else
    echo "TS plugins disabled"
fi
cd $BUILD_DIR
cd paraview_${TARGET}_build
cmake $SOURCE_DIR $opts
make $MAKEOPTS
make install
#cpack -G TGZ
cd ..
rm -rf $BUILD_DIR/paraview_${TARGET}_build $PLUGIN_SOURCE_DIR $SOURCE_DIR
