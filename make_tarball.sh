#!/bin/bash

function make_codehome_tarball() {
    echo "Make CODEHOME tarball"
    local home=$1
    local app=$2
    local version=$3
    local linked_apps=$4
    tmp=tmp$$
    local app_path=$tmp/codes/$app/$app-$version

    mkdir -p $tmp/codes/bin
    mkdir -p $app_path
    for app in ${linked_apps[@]}; do
        (cd $tmp/codes/bin && ln -s ../$app/$app-$version/bin/$app .)
    done
    rsync -a $home/ $app_path
    tarball=$app-$version.tbz2
    (cd $tmp && tar -cjf $tarball codes)
    mv $tmp/$tarball $RELEASE_ROOT && rm -rf $tmp
}
