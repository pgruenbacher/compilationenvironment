. bashrc
NAME="${NAME:-TSPython}"
VERSION="${VERSION:-5.0.0}"
PARAVIEW_VERSION="${PARAVIEW_VERSION:-4.3.1}"
DO_QT="${DO_QT:-FALSE}"
DO_MESA="${DO_MESA:-FALSE}"
DO_FFMPEG="${DO_FFMPEG:-FALSE}"
DO_PARAVIEW="${DO_PARAVIEW:-FALSE}"
MULTIPARAVIEW="${MULTIPARAVIEW:-FALSE}"
DO_PYTHON="${DO_PYTHON:-FALSE}"

DIST_ROOT=${BUILD_ROOT}/Dist
PREFIX="${NAME}-v${VERSION}"

echo "Creating distribution ${NAME}-v${VERSION}"

if [[ $DO_QT == "TRUE" ]]; then
    echo "Do Qt"
    CODE=qt4
    DIST_DIR=$DIST_ROOT/$PREFIX
    INSTALL_DIR=${INSTALL_ROOT}/$CODE
    mkdir -p $DIST_DIR/lib
    cp $INSTALL_DIR/lib/*.so* $DIST_DIR/lib
    [[ -d overlay/$CODE ]] && rsync -a overlay/$CODE/ $DIST_DIR/
fi
if [[ $DO_FFMPEG == "TRUE" ]]; then
    echo "Do ffmpeg"
    CODE=ffmpeg
    DIST_DIR=$DIST_ROOT/$PREFIX
    INSTALL_DIR=${INSTALL_ROOT}/$CODE
    mkdir -p $DIST_DIR/lib $DIST_DIR/bin
    cp $INSTALL_DIR/lib/*.so* $DIST_DIR/lib
    cp $INSTALL_DIR/bin/ffmpeg $DIST_DIR/bin
    [[ -d overlay/$CODE ]] && rsync -a overlay/$CODE/ $DIST_DIR/
fi
if [[ $DO_PARAVIEW == "TRUE" ]]; then
    echo "Do paraview"
    if [[ $DO_QT == "TRUE" ]]; then
        echo "  Paraview GUI"
        CODE=paraview_gui
        NAME=Paraview-GUI
        CODEVERSION=$NAME-v$PARAVIEW_VERSION
        DIST_DIR=$DIST_ROOT/$PREFIX
        if [[ $MULTIPARAVIEW == "TRUE" ]]; then
            DIST_DIR=$DIST_DIR/renderers/$CODEVERSION
        fi
        INSTALL_DIR=${INSTALL_ROOT}/$CODE-v$PARAVIEW_VERSION
        mkdir -p $DIST_DIR
        cp -a $INSTALL_DIR/lib $DIST_DIR/
        cp -a $INSTALL_DIR/bin $DIST_DIR/
        [[ -d overlay/paraview ]] && rsync -a overlay/paraview/ $DIST_DIR/
        PTH=${DIST_DIR}/setup.sh
        LIBVERSION=${PARAVIEW_VERSION:0:3}
        echo "add_python_path \$TS_RENDERER_HOME/lib/paraview-${LIBVERSION}" >> $PTH
        echo "add_python_path \$TS_RENDERER_HOME/lib/paraview-${LIBVERSION}/site-packages" >> $PTH
        echo "add_python_path \$TS_RENDERER_HOME/lib/paraview-${LIBVERSION}/site-packages/vtk" >> $PTH
    else
        if [[ $DO_MESA == "TRUE" ]]; then
            echo "  Paraview OSMESA"
            CODE=paraview_mesa_batch
            NAME=Paraview-CPU
            CODEVERSION=$NAME-v$PARAVIEW_VERSION
            DIST_DIR=$DIST_ROOT/$PREFIX
            if [[ $MULTIPARAVIEW == "TRUE" ]]; then
                DIST_DIR=$DIST_DIR/renderers/$CODEVERSION
            fi
            INSTALL_DIR=${INSTALL_ROOT}/$CODE-v$PARAVIEW_VERSION
            mkdir -p $DIST_DIR
            cp -a $INSTALL_DIR/lib $DIST_DIR/
            cp -a $INSTALL_DIR/bin $DIST_DIR/
            [[ -d overlay/paraview ]] && rsync -a overlay/paraview/ $DIST_DIR/

            MCODE=mesa
            MINSTALL_DIR=${INSTALL_ROOT}/mesa
            cp $MINSTALL_DIR/lib/*.so* $DIST_DIR/lib
            cp $TOOLS_HOME/lib/libLLVM*.so* $DIST_DIR/lib
            PTH=${DIST_DIR}/setup.sh
            LIBVERSION=${PARAVIEW_VERSION:0:3}
            echo "add_python_path \$TS_RENDERER_HOME/lib/paraview-${LIBVERSION}" >> $PTH
            echo "add_python_path \$TS_RENDERER_HOME/lib/paraview-${LIBVERSION}/site-packages" >> $PTH
            echo "add_python_path \$TS_RENDERER_HOME/lib/paraview-${LIBVERSION}/site-packages/vtk" >> $PTH
        fi
        echo "  Paraview batch"
        CODE=paraview_batch
        NAME=Paraview-GPU
        CODEVERSION=$NAME-v$PARAVIEW_VERSION
        DIST_DIR=$DIST_ROOT/$PREFIX
        if [[ $MULTIPARAVIEW == "TRUE" ]]; then
            DIST_DIR=$DIST_DIR/renderers/$CODEVERSION
        fi
        INSTALL_DIR=${INSTALL_ROOT}/$CODE-v$PARAVIEW_VERSION
        mkdir -p $DIST_DIR
        cp -a $INSTALL_DIR/lib $DIST_DIR/
        cp -a $INSTALL_DIR/bin $DIST_DIR/
        [[ -d overlay/paraview ]] && rsync -a overlay/paraview/ $DIST_DIR/
        PTH=${DIST_DIR}/setup.sh
        LIBVERSION=${PARAVIEW_VERSION:0:3}
        echo "add_python_path \$TS_RENDERER_HOME/lib/paraview-${LIBVERSION}" >> $PTH
        echo "add_python_path \$TS_RENDERER_HOME/lib/paraview-${LIBVERSION}/site-packages" >> $PTH
        echo "add_python_path \$TS_RENDERER_HOME/lib/paraview-${LIBVERSION}/site-packages/vtk" >> $PTH
    fi
fi
if [[ $DO_PYTHON == "TRUE" ]]; then
    echo "Do Python"
    CODE=python
    DIST_DIR=$DIST_ROOT/$PREFIX
    INSTALL_DIR=${INSTALL_ROOT}/$CODE
    mkdir -p $DIST_DIR/
    cp -a $INSTALL_DIR/lib $DIST_DIR/
    cp -a $INSTALL_DIR/bin $DIST_DIR/
    [[ -d overlay/$CODE ]] && rsync -a overlay/$CODE/ $DIST_DIR/
    # Copy over MPI libs
    cp $TOOLS_HOME/lib/libmpi*.so* $DIST_DIR/lib
    mkdir $DIST_DIR/lib/openmpi
    cp $TOOLS_HOME/lib/openmpi/*.so* $DIST_DIR/lib/openmpi/
    cp /opt/centos/devtoolset-2/root/usr/lib/gcc/x86_64-redhat-linux/4.8.2/libgfortran.so $DIST_DIR/lib

    find $DIST_DIR -name '*.a' | xargs rm -f
    find $DIST_DIR -name '*.la' | xargs rm -f
    find $DIST_DIR -name '*.pyc' | xargs rm -f

    BASE_DIR=$PWD

    cd $DIST_DIR/bin
    rm -f my* pil* libpng* openssl msql2mysql drop* perror pg* create* c_rehash inno* pg_* png* *db resolve* replace
fi
cd $BASE_DIR
echo "Done"
