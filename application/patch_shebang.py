#!/usr/bin/env python
"""
    

    Copyright TotalSim Ltd, 2015 all rights reserved
        M. Layton (mlayton@totalsim.co.uk)

    The contents of this file are NOT for redistribution
    Please see the README.TotalSim file distributed with this source code
"""
import os, sys

def patch_shebang(file):
    """Patch out the shebang from the file"""
    header = '''#!/bin/sh
""":"
TS_APPLICATION=$(readlink -f ${BASH_SOURCE[0]})
TS_APPLICATION_BIN=$(dirname $TS_APPLICATION)
TS_APPLICATION_HOME=$(dirname $TS_APPLICATION_BIN)
. $TS_APPLICATION_HOME/setup.sh
exec ${TS_PYTHON} "$0" "$@"
":"""'''
    lines = None
    with open(file, 'r') as f:
        lines = f.readlines()
    if 'python' in lines[0]:
        new_lines = [header] + lines[1:]
        with open(file, 'w') as f:
            f.writelines(new_lines)

if __name__ == '__main__':
    print 'Patch shebang'
    for i in sys.argv[1:]:
        path = os.path.abspath(i)
        print '  Patching {}'.format(path)
        patch_shebang(path)