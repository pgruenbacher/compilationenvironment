#!/bin/bash
# 1 - Take a TSPython and create a virtualenv from it
# 2 - Tidy out un-needed files
# 3 - Move some of the real executables out of the way
# 4 - Overlay with the app overlay
# 5 - Install library code and apps using pip & setup.py
# 6 - Make the venv relocatable

# Args: 1 - repo e.g. "mattlayton/test_application"
# Args: 2 - version e.g. 1.0.0
READLINK=readlink
SCRIPT_HOME=$(dirname $($READLINK -f ${BASH_SOURCE[0]}))

REPO_NAME="${1}.git"
VERSION=$2
SKELETON_HOME=$SCRIPT_HOME/skeleton

PIP=pip
PYTHON=python
VIRTUALENV=virtualenv

function clone_app() {
    echo "Clone Application"
    cp -a $1 APPLICATION
}
function clone_repo() {
    local repo_name=$1
    local repo="ssh://git@bitbucket.org/${repo_name}"
    local version=$2
    local tag="v${version}"
    echo "Clone the application repository"
    # Place in into 'MY_APP'
    git clone $repo APPLICATION
    if [[ $tag ]]; then
        (cd APPLICATION; git checkout tags/$tag)
    fi
}
function install_app() {
    echo "Install App"
    local home=$($READLINK -f $1/INSTALL)
    [[ ! -d $home ]] && mkdir $home
    (cd $1/APPLICATION; PYTHONPATH=$home/site-packages $PYTHON setup.py install --home=$home --install-lib=$home/site-packages --install-scripts=$home/bin)
}
function install_packages() {
    echo "Install Packages"
    local home=$1/PACKAGES
    [[ ! -d $home ]] && mkdir $home
    $PIP install --ignore-installed --target=$home -r $1/APPLICATION/requirements.txt
}
function make_relocatable() {
    $SCRIPT_HOME/patch_shebang.py $1/INSTALL/bin/*
}
function create_base() {
    echo "Create base"
    local home=$1/INSTALL
    [[ ! -d $home ]] && mkdir $home
    rsync -a $SKELETON_HOME/ $home
    rsync -a APPLICATION/overlay/ $home --exclude=.git --exclude=*.pyc
    rsync -a PACKAGES/ $home/site-packages
}
function create_codehome_tarball() {
    echo "Create codehome tarball"
    local home=$1
    local app=$2
    local version=$3
    mkdir -p $home/codes/bin
    mkdir -p $home/codes/$app/$app-v$version
    rsync -a $home/INSTALL/ $home/codes/$app/$app-v$version --exclude=*.pyc
    (cd $home/codes/bin; ln -s ../$app/$app-v$version/* .)
    (cd $home; tar -cjf $app-v$version.tbz2 codes;rm -rf codes)
}
function create_tarball() {
    echo "Create tarball"
    local home=$1
    local app=$2
    local version=$3
    mkdir -p $home/$app-v$version
    rsync -a $home/INSTALL/ $home/$app-v$version --exclude=*.pyc
    (cd $home; tar -cjf $app-v$version.tbz2 $app-v$version;rm -rf $app-v$version)
}
clone_app $1
install_packages .
create_base .
install_app .
make_relocatable .

echo "Done"