"""
    

    Copyright TotalSim Ltd, 2015 all rights reserved
        M. Layton (mlayton@totalsim.co.uk)

    The contents of this file are NOT for redistribution
    Please see the README.TotalSim file distributed with this source code
"""
import os
from setuptools import setup, find_packages


__version__ = '1.0.0'

setup(name='demoapp',
    version=__version__,
    description='TotalSim Demo Application',
    # long_description=open('README.md').read(),
    classifiers=["Programming Language :: Python"],
    author='TotalSim Ltd',
    author_email='mlayton@totalsim.co.uk',
    url='',
    keywords='totalsim',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    install_requires=[],
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'demoApp-v%s = demoapp:run_app' % __version__,
        ],
    }
)
