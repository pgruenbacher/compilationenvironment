"""
    Sample application

    Copyright TotalSim Ltd, 2015 all rights reserved
        M. Layton (mlayton@totalsim.co.uk)

    The contents of this file are NOT for redistribution
    Please see the README.TotalSim file distributed with this source code
"""
import os, sys

__version__ = '1.0.0'


def run_app():
    """Running the app"""
    print 'DemoApp-v{}'.format(__version__), ' '.join(sys.argv[1:])

    print '\nTS Environment variables'
    print '========================'
    for k, v in os.environ.items():
        if k.startswith('TS'):
            print '  ', k, '=', v
    print '   PATH = ', os.getenv('PATH')
    print '   LD_LIBRARY_PATH = ', os.getenv('LD_LIBRARY_PATH')

    print '\nTest Paraview'
    print '============='
    try:
        import paraview
        print 'Version', paraview.compatibility.GetVersion()
        from paraview.simple import Cone
        c = Cone()
        print 'Cone', c
    except ImportError:
        print 'Paraview not registered'