#!/bin/bash
#
#   Setup routine
#
#   Copyright TotalSim Ltd, 2014 all rights reserved
#       M Layton (mlayton@totalsim.co.uk)
#
#   The contents of this file are NOT for redistribution
#   Please see the README.TotalSim file distributed with this source code
#
export TS_APPLICATION=${BASH_SOURCE[0]}
export TS_APPLICATION_EXECUTABLE=$(dirname $TS_APPLICATION)
export TS_APPLICATION_HOME=$(readlink -f $TS_APPLICATION_EXECUTABLE)
# Sets the TS_PYTHON variable, pointing to a python executable
. ${TS_APPLICATION_HOME}/config.sh
